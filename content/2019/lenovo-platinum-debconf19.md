Title: Lenovo Platinum Sponsor of DebConf19
Slug: lenovo-platinum-debconf19
Date: 2019-04-30 12:00
Author: Laura Arjona Reina
Artist: Lenovo
Tags: debconf19, debconf, sponsors, lenovo
Status: draft

[![lenovologo](|filename|/images/lenovo.svg)](https://www.lenovo.com/)

We are very pleased to announce that [**Lenovo**](https://www.lenovo.com/)
has committed to support [DebConf19](https://debconf19.debconf.org) as a **Platinum sponsor**.

*"FIXME_QUOTE"*,
said FIXME_PERSON, FIXME_POSITION at Lenovo. *"FIXME_QUOTE_CONTINUATION."*

Lenovo is a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as AR/VR 
devices, smart home/office solutions and data center solutions.

With this commitment as Platinum Sponsor,
Lenovo contributes to make possible our annual conference,
and directly supports the progress of Debian and Free Software
helping to strengthen the community that continues to collaborate on
Debian projects throughout the rest of the year.

Thank you very much Lenovo, for your support of DebConf19!

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf19 website at [https://debconf19.debconf.org](https://debconf19.debconf.org).
