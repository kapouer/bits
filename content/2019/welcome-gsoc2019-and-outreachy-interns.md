Title: Debian welcomes its GSoC 2019 and Outreachy interns
Slug: welcome-gsoc2019-and-outreachy-interns
Date: 
Author: znoteer
Tags: announce, gsoc, outreachy
Status: 


![GSoC logo](|filename|/images/gsoc.jpg)

![Outreachy logo](|filename|/images/outreachy-logo-300x61.png)

We're excited to announce that Debian has selected eight interns to work with us
during the next months: three people for [Outreachy][1], and five for the [Google Summer of Code][2].

[1]: https://www.outreachy.org/alums/
[2]: https://summerofcode.withgoogle.com/organizations/5566947593289728/

Here is the list of projects and the interns who will work on them:

[Android SDK Tools in Debian](https://wiki.debian.org/AndroidTools)

* [Saif Abdul Cassim](https://salsa.debian.org/m36-guest)
* [Katerina](https://FIXME)

[Package Loomio for Debian](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/PackageLoomioForDebian)

* [utkarsh2102](https://FIXME) 

[Debian Cloud Image Finder](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/DebianCloudImageFinder)

* [Arthur Diniz](https://FIXME)

[Debian Patch Porting System](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/DebianPatchPorting)

* [Jaskaran Singh](https://FIXME)

[Continuous Integration](https://wiki.debian.org/SummerOfCode2019/ApprovedProjects/DebianContinuousIntegration)

* [Joyce Z](https://FIXME)
* [Saira Hussain](https://FIXME)
* [Candy Tsai](https://FIXME)

Congratulations and welcome to all the interns!

The Google Summer of Code and Outreachy programs are possible in Debian thanks
to the efforts of Debian developers and contributors that dedicate
part of their free time to mentor interns and outreach tasks.

Join us and help extend Debian! You can follow the interns weekly reports on the
[debian-outreach mailing-list][debian-outreach-ml], chat with us on
[our IRC channel][debian-outreach-irc] or on each project's team mailing lists.

[debian-outreach-ml]: https://lists.debian.org/debian-outreach/ (debian-outreach AT lists.debian.org)
[debian-outreach-irc]: irc://irc.debian.org/#debian-outreach (#debian-outreach on irc.debian.org)
