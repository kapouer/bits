Title: Get involved in DebConf19!
Date: 2019-01-11 12:00
Tags: debconf, debconf19
Slug: debconf19-get-involved
Author: Laura Arjona Reina, Andre Bianchi and Paulo Santana
Artist: DebConf19 Design Team
Status: draft

**[DebConf19](https://debconf19.debconf.org) will be held in
[Curitiba](https://debconf19.debconf.org/about/curitiba/), Brazil from July 21th
to 28th, 2019**. 
It will be preceded by DebCamp, July 14th to 19th, and [Open
Day](https://debconf19.debconf.org/schedule/openday/) on the 20th.

[DebConf](https://www.debconf.org), Debian's annual developers conference,
is an amazing event where Debian contributors from all around the world
gather to present, discuss and work in teams around the Debian operating
system. It is a great opportunity to get to know people responsible for the
success of the project and to witness a respectful and functional distributed
community in action.

We invite everyone to [join us](https://debconf19.debconf.org/contact/) in
organizing DebConf19.
There are different areas where your help could be very valuable,
and we are always looking forward to your ideas.

We are also in the process of contacting potential sponsors from all around the globe.
If you know any organization that could be interested
or who would like to give back resources to FOSS,
please consider handing them
the [sponsorship brochure](https://media.debconf.org/dc19/fundraising/debconf19_sponsorship_brochure_en.pdf)
or [contact the fundraising team](mailto:sponsors@debconf.org) with any leads.

The DebConf team is holding IRC meetings regurlarly.
Have a look at the DebConf19 [website](https://debconf19.debconf.org)
and engage in the IRC channels and the mailing list.

Let’s work together, as every year, on making the best DebConf ever.
We are waiting for you at Curitiba!

![DebConf19 logo](|filename|/images/800px-Debconf19-horizontal.png)
