Title: Backports integrated into the main archive
Date: 2013-03-18 15:00
Tags: announce, backports
Slug: backports-integrated-into-the-main-archive
Author: Ana Guerrero
Status: published

**This is a repost from [Gerfried Fuchs's post](http://rhonda.deb.at/blog/2013/03/18#backports-integrated-into-the-main-archive)**

Dear users and supporters of the backports service!

The Backports Team is pleased to announce the next important step on getting
backports more integrated. People who are reading [debian-infrastructure-
announce](http://lists.debian.org/debian-infrastructure-announce/) will have
seen that there was an archive maintenance last weekend: starting with wheezy-
backports the packages will be accessible from the regular pool instead of a
separate one, and all backports uploads will be processed through the regular
upload queue (including those for squeeze-backports and squeeze-backports-
sloppy).

## For Users

What exactly does that mean for you? For users of wheezy, the sources.list
entry will be different, a simple substitute of squeeze for wheezy won't work.
The new format is:

> deb http://ftp.debian.org/debian/ wheezy-backports main

So it is debian instead of debian-backports, and offered through the regular
mirror network. Feel invited to check your regular mirror if it carries
backports and pull from there.

## For Contributers

What does it mean for contributing developers? Uploads for backports are no
longer to be pushed to backports-master but to ftp.upload.debian.org, like any
other regular package. Also, given that the packages are served from the same
archive install there is no need to include the original tarball in the upload
any longer because the archive knows it (Squeeze and beyond).

Also, given that the upload goes to the same upload queue, there is only one
keyring used anymore, so no more pain with expired or replaced keys. We though
still keep the rule of adding your UID to an ACL list (this also includes DM
additions). This is mostly only to give us the chance to remind you that
uploads to backports are directly available for installation onto stable
systems and you thus should take special care there. We carefully tried to
take over the old ACLs, in case you can't upload anymore, please [tell
us](mailto:team@backports.debian.org) so we can look into the issue.

I've mentioned wheezy-backports (and squeeze-backports-sloppy) a few times
here already, and you might wonder when it will be available. Technically, it
is available from now on. Practically, while you could already upload to it,
the set up of the buildd network is more painful than expected, so please
allow the Buildd Team some days for setting them up.

The upload rules for wheezy-backports are the same: packages that are in the
next suite are accepted. Given that Jessie isn't created yet, we want you to
think about whether the package you want to upload will go into Jessie final,
and that you are taking a closer look once Jessie is created and the package
entered there about the upgradeability. For the time until the suite is
available, you can see this as relaxed upload rule.

The same goes for squeeze-backports-sloppy: packages from two suites after
Squeeze are acceptable, which turns it into the same relaxed rule as wheezy-
backports above. Please also keep in mind that uploads to squeeze-backports-
sloppy usually should be accompanied by uploads to wheezy-backports so people
are able to upgrade from squeeze-backports-sloppy to wheezy with wheezy-
backports.

## Thanks

Finally, we want to thank the FTP-Master Team for their fine work on making
this happen.

The documentation on [backports-master](http://backports-master.debian.org/)
has been updated, and in case of any doubt or question, feel free to ask them
on either the [debian-backports mailinglist](http://lists.debian.org/debian-
backports/), or in case of sensitive topics [ask
us](mailto:team@backports.debian.org) directly.

Enjoy!

Rhonda for the Backports Team

