Title: Universo paralelo desconhecido usa Debian
Slug: unknown-parallel-universe-uses-debian
Date: 2017-04-01 15:30
Author: Debian Publicity Team
Tags: debian, announce
Status: published
Lang: pt-BR
Translator: Samuel Henrique

**This post was an April Fools' Day joke.**

As agências espaciais participantes da Estação Espacial Internacional
([International Space Station (ISS)](https://en.wikipedia.org/wiki/International_Space_Station_program))
reportaram que um laptop acidentalmente jogado ao espaço junto com o lixo em
2013 pode ter contactado um Universo paralelo.
[Esse laptop estava rodando Debian 6](https://phys.org/news/2013-05-international-space-station-laptop-migration.html),
a equipe de engenharia da ISS conseguiu rastrear o caminho dele
através cosmos. No início de Janeiro, o sinal do laptop foi perdido e
recuperado duas semanas depois no mesmo local. Membros da ISS suspeitam
que o laptop possa ter cruzado um [buraco de minhoca](https://pt.wikipedia.org/wiki/Buraco_de_minhoca)
chegando em um Universo paralelo de onde "alguém" o enviou de volta mais tarde.

Eventualmente o laptop foi recuperado, e em uma primeira análise, a equipe de
engenharia da ISS descobriu que ele estava com dual boot: uma partição rodando
a instalação Debian feita por eles mesmos e uma segunda partição rodando o que
parece ser um fork, ou [derivação](https://wiki.debian.org/Derivatives/) do Debian
totalmente desconhecido até agora.

Esta equipe de engenharia tem entrado em contato com o Projeto Debian nas últimas semanas
e um grupo foi formado com representantes de diferentes times do Debian para dar
início a um estudo sobre essa nova derivada Debian. A partir de resultados
iniciais, podemos orgulhosamente dizer que alguém (ou um grupo de alguéns) em
um Universo paralelo entende os computadores da Terra suficientemente bem para:

* Clonar um sistema Debian existente em uma nova partição e configurar dual boot
utilizando Grub.
* Alterar o plano de fundo anterior do
[tema Spacefun](https://wiki.debian.org/DebianArt/Themes/SpaceFun)
para um usando cores do arco-íris.
* Fazer um Fork de todos os pacotes cujo
[código-fonte](https://sources.debian.net/) estava presente no sistema Debian
inicial, fazer patches para múltiplos bugs destes pacotes e também alguns para
problemas de segurança realmente complicados.
* Adicionar dez novas línguas que não correspondem a nada já visto na Terra, com
tradução completa para quatro delas.
* Uma cópia do
[repositório do site Debian](https://anonscm.debian.org/viewvc/webwml/),
migrado para o sistema de controle de versionamento git e rodando perfeitamente,
foi encontrado na pasta _/home/earth0/Documentos_. Esse novo repositório
contém código para mostrar o Debian [micronews](https://micronews.debian.org/)
na sua homepage, além de outras melhorias, mantendo a conduta de não precisar
de JavaScript e provendo um bom modelo de controle de traduções
atualizadas/desatualizadas, parecido com o existente no Debian.

O trabalho para conhecer melhor esse novo Universo e encontrar um método de
comunicação com eles apenas começou; todos os usuários e contribuidores Debian
estão convidados a unir esforços para estudar o sistema operacional encontrado.
Nós queremos preparar a nossa comunidade e o nosso Universo para viver e
trabalhar pacificamente and respeitosamente com as comunidades desse Universo
paralelo, no verdadeiro espírito do software livre

Nas próximas semanas uma resolução geral será proposta para atualizar o nosso
motto para "o sistema operacional multiversal".
