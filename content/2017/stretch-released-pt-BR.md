Title: Foi lançado o Debian 9.0 Stretch!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: pt-BR
Translator: Adriano Rafael Gomes

![Alt Foi lançada a Stretch](|filename|/images/banner_stretch.png)

Deixe-se abraçar pelo polvo de borracha roxo de brinquedo! Nós estamos felizes em anunciar
o lançamento do Debian 9.0, codinome *Stretch*.

**Quer instalá-lo?**
Escolha a sua [mídia de instalação](https://www.debian.org/distrib/) favorita entre discos Blu-ray, DVDs,
CDs e pendrives USB. Então leia o [manual de instalação](https://www.debian.org/releases/stretch/installmanual).

**Já é um usuário Debian feliz e quer apenas atualizar?**
Você pode atualizar facilmente a partir do seu Debian 8 atualmente instalado,
por favor, leia as [notas de lançamento](https://www.debian.org/releases/stretch/releasenotes).

**Você quer celebrar o lançamento?**
Compartilhe o [banner deste blog](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) no seu blog ou no seu site web!
