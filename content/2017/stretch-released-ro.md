Title: Debian 9.0 Stretch a fost lansată!
Date: 2017-06-18 08:25
Tags: stretch
Slug: stretch-released
Author: Ana Guerrero Lopez and Laura Arjona Reina
Status: published
Lang: ro
Translator: Ionel Mugurel Ciobîcă

![Alt Stretch a fost lansată](|filename|/images/banner_stretch.png)

Lasă-te îmbrățișat de jucăria caracatiță de cauciuc mov! Suntem fericiți să anunțăm
lansarea Debian 9.0, denumit *Stretch*.

**Vrei să o instalezi?**
Alege varianta favorită [installation media](https://www.debian.org/distrib/) dintre discuri Blu-ray,
DVD-uri, CD-uri sau stickuri USB. Citește apoi [ghidul de instalare](https://www.debian.org/releases/stretch/installmanual).

**Ești deja un utilizator fericit de Debian și vrei să faci upgrade?**
Poți face ușor upgrade de la instalarea ta de Debian 8 Jessie,
citește [notele de lansare](https://www.debian.org/releases/stretch/releasenotes).

**Vrei să sărbătorești cu noi lansarea?**
Distribuie [bannerul din acest blog](https://wiki.debian.org/DebianArt/Themes/softWaves?action=AttachFile&do=view&target=wikiBanner.png) în blogul tău sau pe pagina ta!
