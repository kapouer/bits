# Sources for new DDs and DMs posts.
* debian-keyring changelog check keyring changelog: https://salsa.debian.org/debian-keyring/keyring/raw/master/debian/changelog
* RT tickets: https://rt.debian.org/User/Summary.html?id=3602

# Month Names

en: January, February, March, April, May, June
es: enero, febrero, marzo, abril, mayo, junio
fr: janvier, février, mars, avril, mai, juin
ca: gener, febrer, març, abril, maig, juny
pt: janeiro, fevereiro, março, abril, maio, junho

en: July, August, September, October, November, December
es: julio, agosto, septiembre, octubre, noviembre, diciembre
fr: juillet, août, septembre, octobre, novembre, décembre
ca: juliol, agost, setembre, octubre, novembre, desembre
pt: julho, agosto, setembro, outubro, novembro, dezembro
